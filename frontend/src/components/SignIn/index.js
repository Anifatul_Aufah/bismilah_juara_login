import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import Footer from '../Footer'
import Header from '../Header'
import './../routes'

 class SignIn extends Component {
    render () {
      return (
        <div> 
        <Header/>   
          <meta charSet="utf-8" />
          <title>Wokiee - Responsive HTML5 Template</title>
          <meta name="keywords" content="HTML5 Template" />
          <meta name="description" content="Wokiee - Responsive HTML5 Template" />
          <meta name="author" content="wokiee" />
          <link rel="shortcut icon" href="favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <link rel="stylesheet" href="css/theme.css" />
          <div id="loader-wrapper">
            <div id="loader">
              <div className="dot" />
              <div className="dot" />
              <div className="dot" />
              <div className="dot" />
              <div className="dot" />
              <div className="dot" />
              <div className="dot" />
            </div>
          </div>
          <div className="tt-breadcrumb">
            <div className="container">
              <ul>
                <li><a href="index.html">Home</a></li>
                <li>Login</li>
              </ul>
            </div>
          </div>
          <div id="tt-pageContent">
            <div className="container-indent">
              <div className="container">
                <h1 className="tt-title-subpages noborder">SUDAH PUNYA AKUN ?</h1>
                <div className="tt-login-form">
                  <div className="row">
                    <div className="col-xs-12 col-md-6">
                      <div className="tt-item">
                        <h2 className="tt-title">NEW CUSTOMER</h2>
                        <p>
                          By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.
                        </p>
                        <div className="form-group">
                          <Link to="/SignUp" className="btn btn-top btn-border">CREATE AN ACCOUNT</Link>
                        </div>
                      </div>
                    </div>
                    <div className="col-xs-12 col-md-6">
                      <div className="tt-item">
                        <h2 className="tt-title">LOGIN</h2>
                        If you have an account with us, please log in.
                        <div className="form-default form-top">
                          <form id="customer_login" method="post" noValidate="novalidate">
                            <div className="form-group">
                              <label htmlFor="loginInputName">USERNAME OR E-MAIL *</label>
                              <div className="tt-required">* Required Fields</div>
                              <input type="text" name="username"className="form-control" id="loginInputName" placeholder="Enter Username or E-mail" required/>
                            </div>
                            <div className="form-group">
                              <label htmlFor="loginInputEmail">PASSWORD *</label>
                              <input type="text" name="password" className="form-control" id="loginInputEmail" placeholder="Enter Password" required/>
                            </div>
                            <div className="row">
                              <div className="col-auto mr-auto">
                                <div className="form-group">
                                  <button className="btn btn-border" type="submit">LOGIN</button>
                                </div>
                              </div>
                              <div className="col-auto align-self-end">
                                <div className="form-group">
                                  <ul className="additional-links">
                                    <li><Link to="/ForgotPassword">Lupa Password ?</Link></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* form validation and sending to mail */}
          <Footer/>
        </div>
      );
    }
  }
  export default SignIn;