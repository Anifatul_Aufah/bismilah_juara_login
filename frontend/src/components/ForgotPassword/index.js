import React, { Component } from 'react';

export default class ForgotPassword extends Component {
    render () {
      return (
        <div>
        <meta charSet="utf-8" />
        <title>Wokiee - Responsive HTML5 Template</title>
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Wokiee - Responsive HTML5 Template" />
        <meta name="author" content="wokiee" />
        <link rel="shortcut icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="css/theme.css" />
        <div id="loader-wrapper">
          <div id="loader">
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
          </div>
        </div>
        <div className="tt-breadcrumb">
          <div className="container">
            <ul>
              <li><a href="index.html">Home</a></li>
              <li>Reset Password</li>
            </ul>
          </div>
        </div>
        <div id="tt-pageContent">
          <div className="container-indent">
            <div className="container">
              <h1 className="tt-title-subpages noborder">Lupa Password?</h1>
              <div className="tt-login-form">
                <div className="row">
                  <div className="col-xs-12 col-md-8">
                    <div className="tt-item">
                      <h2 className="tt-title">Reset Password</h2>
                      Reset Password akan dikirim ke Email Anda
                      <div className="form-default form-top">
                        <form id="customer_login" method="post" noValidate="novalidate">
                          <div className="form-group">
                            <label htmlFor="loginInputEmail">EMAIL *</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="email" name="email" className="form-control" id="loginInputEmail" placeholder="Masukkan Email Anda" />
                          </div>
                          <div className="row">
                            <div className="col-auto mr-auto">
                              <div className="form-group">
                                <button className="btn btn-border" type="submit">RESET</button>
                              </div>
                            </div>
                          </div>
                        </form></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* form validation and sending to mail */}
      </div>
    );
  }
}
