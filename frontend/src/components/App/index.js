import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import SignUp from '../SignUp';
import SignIn from '../SignIn';
import ForgotPassword from '../ForgotPassword';
import * as ROUTES from '../routes';

const App = () => (
  <Router>
    <div>

      <Route exact path={ROUTES.SIGN_IN} component={SignIn} />
      <Route path={ROUTES.SIGN_UP} component={SignUp} />
      <Route
        path={ROUTES.FORGOT_PASSWORD}
        component={ForgotPassword}
      />
    </div>
  </Router>
);

export default (App);
