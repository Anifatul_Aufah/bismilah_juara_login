import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import Footer from '../Footer'
import Header from '../Header'
import './../routes'
import firebase from 'firebase';
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";

firebase.initializeApp({
  apiKey:"AIzaSyATa8Eiy0G653ziI2GdAVUAkajOkBKFKFI",
  authDomain:"fir-auth-react-77383.firebaseapp.com"
});

export default class SignUp extends Component {
  state = { isSignedIn : false }
      uiConfig = {
      signInFlow: "popup",
      signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID
      ],
      callbacks: {
        signInSuccess: () => false
      }
    };
    componentDidMount = () => {
      firebase.auth().onAuthStateChanged(user => {
        this.setState({isSignedIn:!!user})
      })
    };  
    render () {
      return (
        <div>
        <Header />
        <meta charSet="utf-8" />
        <title>Wokiee - Responsive HTML5 Template</title>
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Wokiee - Responsive HTML5 Template" />
        <meta name="author" content="wokiee" />
        <link rel="shortcut icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="css/theme.css" />
        <div id="loader-wrapper">
          <div id="loader">
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
            <div className="dot" />
          </div>
        </div>
        <div className="tt-breadcrumb">
          <div className="container">
            <ul>
              <li><a href="index.html">Home</a></li>
              <li>Register</li>
            </ul>
          </div>
        </div>
        <div id="tt-pageContent">
          <div className="container-indent">
            <div className="container">
              <h1 className="tt-title-subpages noborder">Belum Punya Akun?</h1>
              <div className="tt-login-form">
                <div className="row">
                  <div className="col-xs-12 col-md-6">
                    <div className="tt-item">
                      <h2 className="tt-title"><center>Daftar Dengan Media Sosial</center></h2>
                      <p>
                        anda akan tersambung dengan akun sosial media sehingga login lebih cepat.
                        <br/>
                        {this.state.isSignedIn ? (
                        <span>
                        <div>Signed In!</div>
                        <button onClick={() => firebase.auth().signOut()}>Sign out!</button>
                        </span>
                        ): (
                        <StyledFirebaseAuth
                        uiConfig={this.uiConfig}
                        firebaseAuth={firebase.auth()}
                        />  
                        )}
                      </p>
                    </div>
                  </div>
                  <div className="col-xs-12 col-md-6">
                    <div className="tt-item">
                      <h2 className="tt-title">REGISTER</h2>
                      Jika belum punya akun silahkan Daftar terlebih dahulu
                      <div className="form-default form-top">
                        <form id="customer_login" method="post" noValidate="novalidate">
                          <div className="form-group">
                            <label htmlFor="loginInputName">USERNAME *</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="text" name="username" className="form-control" id="loginInputName" placeholder="Masukkan Username" required/>
                          </div>
                          <div className="form-group">
                            <label htmlFor="loginInputFirstname">FIRSTNAME *</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="text" name="firstname" className="form-control" id="loginInputFirstname" placeholder="Masukkan Nama Depan Anda" required/>
                          </div>
                          <div className="form-group">
                            <label htmlFor="loginInputLastname">LASTNAME *</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="text" name="lastname" className="form-control" id="loginInputLastname" placeholder="Masukkan Nama Belakang Anda" required />
                          </div>
                          <div className="form-group">
                            <label htmlFor="loginInputEmail">EMAIL *</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="email" name="email" className="form-control" id="loginInputEmail" placeholder="Masukkan Email Anda" required/>
                          </div>
                          <div className="form-group">
                            <label htmlFor="loginInputEmail2">EMAIL CONFIRM *</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="email" name="emailConf" className="form-control" id="loginInputEmail2" placeholder="Masukkan Email Anda Sekali Lagi" required/>
                          </div>
                          <div className="form-group">
                            <label htmlFor="loginInputPassword">PASSWORD *</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="password" name="password" className="form-control" id="loginInputPassword" placeholder="Masukkan Password Anda" required/>
                          </div>
                          <div className="form-group">
                            <label htmlFor="loginInputPassword2">PASSWORD CONFIRM*</label>
                            <div className="tt-required">* Required Fields</div>
                            <input type="password" name="passwordConf" className="form-control" id="loginInputPassword2" placeholder="Masukkan Password Anda Sekali Lagi" />
                          </div>
                          <div className="row">
                            <div className="col-auto mr-auto">
                              <div className="form-group">
                                <button className="btn btn-border" type="submit">LOGIN</button>
                              </div>
                            </div>
                            <div className="col-auto align-self-end">
                              <div className="form-group">
                                <ul className="additional-links">
                                  <li><Link to="/ForgotPassword">Lupa password?</Link></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* form validation and sending to mail */}
        <Footer />
      </div>
    );
  }
}
